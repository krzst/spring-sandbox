package com.kris.spring.sandbox;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServletConfig {
    @Bean
    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> containerCustomizer() {
        return (container -> {
            String port = System.getenv("PORT");
            if (port != null) {
                container.setPort(Integer.valueOf(port));

            }
        });
    }
}
