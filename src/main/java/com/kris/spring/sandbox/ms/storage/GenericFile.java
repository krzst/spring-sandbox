package com.kris.spring.sandbox.ms.storage;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.File;

@Getter
@Setter
@Builder
@ToString
public class GenericFile {

    @NotNull
    private String body;

    @NotNull
    private String name;

    private GenericFile() {

    }

    private GenericFile(String body, String name) {
        this.body = body;
        this.name = name;
    }
}
