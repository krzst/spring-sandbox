package com.kris.spring.sandbox.ms.user;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Setter
@Builder
@ToString
@Document(collection = "users")
@CompoundIndex(name = "login_unique_idx", def = "{'login': 1}", unique = true)
public class User {

    @Id
    private String id;

    @NotNull
    private String login;

    private User() {

    }

    private User(final String id, final String login) {
        this.id = id;
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}