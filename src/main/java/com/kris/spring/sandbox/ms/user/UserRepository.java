package com.kris.spring.sandbox.ms.user;

import com.kris.spring.sandbox.ms.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    User findByLogin(String login);

}
