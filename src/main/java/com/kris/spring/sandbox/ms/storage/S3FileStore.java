package com.kris.spring.sandbox.ms.storage;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.UploadPartRequest;
import com.amazonaws.util.IOUtils;
import com.kris.spring.sandbox.ms.storage.aws.S3Config;
import com.kris.spring.sandbox.ms.util.ValidationMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Slf4j
@Component
public class S3FileStore implements FileStore {

    private S3Config s3Config;

    @Autowired
    private S3FileStore(S3Config config) {
        this.s3Config = config;
    }

    @Override
    public Boolean upload(GenericFile genericFile) {
        Assert.notNull(genericFile, ValidationMsg.NOT_NULL);

        boolean result = Boolean.FALSE;
        try {
            s3Config.getS3().putObject(s3Config.getBucket(), genericFile.getName(), genericFile.getBody());

            result = Boolean.TRUE;
        }
        catch(AmazonServiceException e) {
            log.warn("Upload warn", e);
        }
        catch(SdkClientException e) {
            log.error("Upload error", e);
        }

        return result;
    }

    @Override
    public GenericFile download(String name) {
        Assert.notNull(name, ValidationMsg.NOT_NULL);

        S3Object s3Object = s3Config.getS3().getObject(s3Config.getBucket(), name);
        S3ObjectInputStream objectContent = s3Object.getObjectContent();
        String body = null;
        try {
            body = IOUtils.toString(objectContent);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        GenericFile genericFile = GenericFile.builder().name(name).body(body).build();

        return genericFile;
    }

}
