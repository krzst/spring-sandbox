package com.kris.spring.sandbox.ms.storage;

import java.io.File;

public interface FileStore {

    Boolean upload(GenericFile genericFile);

    GenericFile download(String name);

}
