package com.kris.spring.sandbox.ms.weather.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class WeatherConsumer {

    @Value("${open.weather.api.url.5days}")
    private String weatherApiUrl5Days;

    @Scheduled(fixedRate = 29 * 60 * 1000)
    public void pullWeather() {
        log.info("Pulling data from: " + weatherApiUrl5Days);
    }


}
