package com.kris.spring.sandbox.ms.storage;

import com.amazonaws.util.IOUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping(path = "/store")
@Slf4j
public class StoreController {

    @Autowired
    private S3FileStore s3FileStore;

    @PostMapping(path = "/{type}/name/{filename}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void add(@PathVariable(name = "type") String type, @PathVariable(name = "filename") String filename, @RequestParam("file") MultipartFile file) throws IOException {
        String body = IOUtils.toString(file.getInputStream());


        GenericFile genericFile = GenericFile.builder().name(filename).body(body).build();

        Boolean upload = s3FileStore.upload(genericFile);

        log.info(upload.toString());
    }
}
