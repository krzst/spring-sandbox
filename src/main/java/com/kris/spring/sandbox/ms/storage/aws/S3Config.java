package com.kris.spring.sandbox.ms.storage.aws;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class S3Config {

    @Value("${aws.key.id}")
    private String keyId;

    @Value("${aws.key.secret}")
    private String secretId;

    @Value("${aws.bucket}")
    private String bucket;

    @Value("${aws.region}")
    private String region;

    private AmazonS3 s3;

    private S3Config() {

    }

    @PostConstruct
    private void init() {
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(keyId, secretId);

        s3 = AmazonS3ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    public AmazonS3 getS3() {
        return s3;
    }

    public String getBucket() {
        return bucket;
    }
}

